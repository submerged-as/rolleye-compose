#!/usr/bin/env python

from setuptools import setup, find_packages
import os

thelibFolder = os.path.dirname(os.path.realpath(__file__))
requirementPath = thelibFolder + "/requirements.txt"
install_requires = []
if os.path.isfile(requirementPath):
    with open(requirementPath) as f:
        install_requires = f.read().splitlines()


setup(
    name="py_rolleye_compose",
    version="0.0.0",
    long_description=__doc__,
    packages=find_packages(include=["py_rolleye_compose"]),
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires,
    entry_points={
        "console_scripts": [
            "service-update-compose=py_rolleye_compose.auto_update:update_compose",
            "update-compose=py_rolleye_compose.auto_update:update_process",
            "update-rabbitmq=py_rolleye_compose.auto_update:install_rabbitmq",
            "update-docker-compose=py_rolleye_compose.update_compose:update_docker_composes",
            "build-docker-compose=py_rolleye_compose.update_compose:build_docker_compose",
        ],
    },
)
