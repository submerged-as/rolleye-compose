import click
from pathlib import Path
from sh import docker_compose
from sh import docker
import subprocess


working_dir = Path(__file__).resolve().parent.parent
print(working_dir)
compose_dir = Path(working_dir)

@click.command()
def build_docker_compose():
    b_docker_c()
def b_docker_c():
    """
    Builds the docker-compose.yml file by combining different YAML files
    based on the existence of the 'rolleye-cam.service'.
    """

    # Find the root of the package
    script_dir = Path(__file__).resolve()
    while script_dir.parent.joinpath("__init__.py").exists():
        script_dir = script_dir.parent
    script_dir = script_dir.parent.parent  # Now at the package root level

    # Construct the full paths to the files
    networks_file = script_dir / "composes" / "networks.yml"
    no_cam_file = script_dir / "composes" / "no-cam.yml"
    producer_file = script_dir / "composes" / "producer.yml"
    output_file = script_dir / "docker-compose.yml"

    missing_files = []
    for file in [networks_file, no_cam_file, producer_file]:
        if not file.exists():
            missing_files.append(str(file))

    if missing_files:
        print("Error: The following required file(s) do not exist:")
        for missing_file in missing_files:
            print(f"  - {missing_file}")
        return False

    # Check if the "rolleye-cam.service" exists (using subprocess)
    try:
        subprocess.check_call([
            "systemctl", "status", "rolleye-cam.service"
        ], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        service_exists = True
    except subprocess.CalledProcessError:
        service_exists = False

    # Combine files based on the existence of the service
    try:
        with open(str(output_file), "w") as out_file:  # Convert Path to string
            if service_exists:
                # If the service exists, combine only no-cam.yml and networks.yml
                files_to_combine = [no_cam_file, networks_file]
                print("rolleye-cam.service exists. Combining no-cam.yml and networks.yml into docker-compose.yml")
            else:
                # If the service doesn't exist, combine all three files
                files_to_combine = [no_cam_file, producer_file, networks_file]
                print("Combining all files into docker-compose.yml")

            for file in files_to_combine:
                with open(str(file), "r") as f:  # Convert Path to string
                    out_file.write(f.read())
        return True

    except Exception as e:
        print("Error: Failed to write to {}. {}".format(output_file, e))
        return False


def update_docker_compose():
    b_docker_c()
    print("pulling")
    print(docker_compose("-f", str(Path(compose_dir, "docker-compose.yml")), "pull"))
    print("downing")
    print(docker_compose("-f", str(Path(compose_dir, "docker-compose.yml")), "down"))
    
    # stop and delete all running containers
    print("stopping and deleting all running containers")
    running_containers = docker('ps', '-q').splitlines()
    for container in running_containers:
        docker('rm', container)

    print("starting the docker-compose")
    print(
        docker_compose("-f", str(Path(compose_dir, "docker-compose.yml")), "up", "-d")
    )
    return True


@click.command()
def start_all_docker_compose():
    entries = Path(compose_dir).glob("*")
    for entry in entries:
        if entry.is_dir() and not entry.name.startswith("."):
            print(
                docker_compose("-f", str(Path(entry, "docker-compose.yml")), "up", "-d")
            )
            print(f"started {entry.name}")

@click.command()
def update_docker_composes():
    update_docker_compose()