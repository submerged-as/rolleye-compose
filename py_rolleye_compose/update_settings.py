import json
from pathlib import Path
import click

working_dir = Path(__file__).resolve().parent.parent
print(working_dir)
new_settings_dir = Path(working_dir, "settings")
settings_dir = "/settings"


def update_scheduler():
    print("updating schedules")
    with open(Path(new_settings_dir, "default-schedule.json"), "r") as f:
        new_settings = json.load(f)
    manual_path = Path(settings_dir, "manual_schedules.json")
    if not manual_path.exists():
        manual_schedules = [
            {
                "period": 1,
                "every": "day",
                "time": "09:00",
                "function": "dummy_func",
                "function argument": "Its nine o`clock",
            }
        ]
        with open(manual_path, "w") as f:
            json.dump(manual_schedules, f, indent=4)
    else:
        with open(manual_path, "r") as f:
            manual_schedules = json.load(f)
    with open(Path(settings_dir, "scheduled_jobs.json"), "w") as f:
        json.dump(new_settings + manual_schedules, f, indent=4)
    print("Updated schedules")


def update_camera_settings():
    print("updating camera settings")
    with open(Path(new_settings_dir, "camera_settings.json"), "r") as f:
        camera_settings = json.load(f)
    with open(Path(settings_dir, "camera_settings.json"), "w") as f:
        json.dump(camera_settings, f, indent=4)


@click.command()
def initialize_settings():
    update_camera_settings()
    update_scheduler()
