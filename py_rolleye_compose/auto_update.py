from sh import git
import time
from pathlib import Path
import click
import random
import string
import subprocess


working_dir = Path(__file__).resolve().parent.parent
print(working_dir)


def check_for_updates():
    try:
        p = git("-C", str(working_dir), "pull").stdout.decode("utf8")
    except Exception as err:
        print("problems pulling")
        print(err)
        return "False"
    return p

@click.command()
def service_update_compose():
    while True:
        update_process_()
        time.sleep(3600)

@click.command()
def update_process():
    update_process_()

def update_process_():
    p = check_for_updates()
    if "Already up to date" not in p:
        # need to import modules here in case they are updated.
        from .update_compose import update_docker_compose

        print("Update found")
        update_install_rabbitmq()
        update_docker_compose()
        print("kom her")
        try:
            # Execute the systemctl daemon-reload command
            subprocess.run(['systemctl', 'daemon-reload'], check=True)
            print("Daemon reloaded successfully.")
        except subprocess.CalledProcessError as e:
            print(f"Error reloading daemon: {e}")
        except FileNotFoundError:
            print("systemctl command not found.")

        subprocess.run(str(Path(working_dir,'install.sh')))
    else:
        print("no updates found")


def update_install_rabbitmq():

    # Define constants
    REPO_DIR = Path("/home/submerged/rolleye-compose")
    ENV_FILE = REPO_DIR / ".env"
    RABBITMQ_USER = "rabbit"

    # Function to generate a random password
    def generate_password(length=16):
        characters = string.ascii_letters + string.digits
        return ''.join(random.choice(characters) for _ in range(length))

    # Check if the .env file exists
    if not ENV_FILE.exists():
        # Generate password and write to .env file
        rabbitmq_password = generate_password()

        ENV_FILE.write_text(f"RABBITMQ_USER={RABBITMQ_USER}\nRABBITMQ_PASSWORD={rabbitmq_password}\n")

        print(".env file created with RABBITMQ_USER and RABBITMQ_PASSWORD.")
    else:
        print(".env file already exists.")

@click.command()
def install_rabbitmq():
    update_install_rabbitmq()