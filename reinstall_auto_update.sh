#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo $DIR

pip3 install -r ${DIR}/requirements.txt

python3 setup.py develop

rm -f /etc/systemd/system/auto-update-compose.service
ln -s ${DIR}/auto-update-compose.service /etc/systemd/system/auto-update-compose.service
systemctl enable auto-update-compose.service
systemctl start auto-update-compose.service
