#!/bin/bash

# Get the directory where the script is located
script_dir=$(dirname "$0")

# Construct the full paths to the files
networks_file="$script_dir/composes/networks.yml"
no_cam_file="$script_dir/composes/no-cam.yml"
producer_file="$script_dir/composes/producer.yml"

# Check if the files exist
if [ ! -f "$networks_file" ] || [ ! -f "$no_cam_file" ] || [ ! -f "$producer_file" ]; then
  echo "Error: One or more files do not exist."
  exit 1
fi

# Check if the "rolleye-cam.service" exists
if systemctl status rolleye-cam.service > /dev/null 2>&1; then
  # If the service exists, combine only networks.yml and producer.yml
  cat "$no-cam_file" "$networks_file" > "$script_dir/docker-compose.yml"
  echo "rolleye-cam.service exists. no-cam.yml and networks.yml combined into docker-compose.yml"
else
  # If the service doesn't exist, combine all three files
  cat "$no_cam_file" "$producer_file" "$networks_file" > "$script_dir/docker-compose.yml"
  echo "Files combined successfully into combined.yml"
fi