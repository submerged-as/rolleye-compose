#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo $DIR

pip3 install -r ${DIR}/requirements.txt

python3 setup.py develop


if systemctl list-unit-files | grep -q auto-update-compose.service; then
    # Service exists
    if systemctl status auto-update-compose.service > /dev/null 2>&1; then
        echo "auto-update-running"
    else
        echo "restarting auto-update"
        systemctl restart auto-update-compose.service
    fi
fi

if systemctl list-unit-files | grep -q auto-update-compose.service; then
    # Service exists
    if systemctl status auto-update-compose.service > /dev/null 2>&1; then
        echo "auto-update-running"
    else
        echo "restarting auto-update"
        systemctl restart auto-update-compose.service
    fi
else
    rm -f /etc/systemd/system/auto-update-compose.service
    ln -s ${DIR}/auto-update-compose.service /etc/systemd/system/auto-update-compose.service
    systemctl enable auto-update-compose.service
    systemctl start auto-update-compose.service
fi

${DIR}/docker-prune/install-docker-prune.sh

# Check if the "rolleye-cam.service" exists
if systemctl list-unit-files | grep -q rolleye-cam.service; then

    if systemctl status rolleye-cam.service > /dev/null 2>&1; then
        echo "running old camera app"
    else
        echo "restarting rolleye-cam"
        systemctl restart rolleye-cam.service
    fi
fi

if systemctl list-unit-files | grep -q rolleye-cam.service; then

    if systemctl status rolleye-cam.service > /dev/null 2>&1; then
        echo "running old camera app"
    else
        echo "restarting rolleye-cam"
        systemctl restart rolleye-cam.service
    fi
else
    rm -f /etc/systemd/system/nvargus_restarter.service
    ln -s ${DIR}/nvargus_restarter.service /etc/systemd/system/nvargus_restarter.service
    systemctl enable nvargus_restarter.service
    systemctl start nvargus_restarter.service
    echo "running producer camera app"
fi

# Check if the "cp-raw.service" exists This has been moved to docker and should be cleared here. 
if systemctl list-unit-files | grep -q cp-raw.service; then
    systemctl stop cp-raw.service
    systemctl disable cp-raw.service
fi
    
update-rabbitmq

systemctl daemon-reload

