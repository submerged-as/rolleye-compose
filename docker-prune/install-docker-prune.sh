DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo $DIR

cp $DIR/log-rotate /etc/logrotate.d/disk_space_checker

logrotate --force /etc/logrotate.d/disk_space_checker


if systemctl list-unit-files | grep -q docker-prune.service; then
    # Service exists
    if systemctl status docker-prune.service > /dev/null 2>&1; then
        echo "auto-update-running"
    else
        echo "restarting auto-update"
        systemctl restart docker-prune.service
    fi
else
    rm -f /etc/systemd/system/docker-prune.service
    ln -s ${DIR}/docker-prune.service /etc/systemd/system/docker-prune.service
    systemctl enable docker-prune.service
    systemctl start docker-prune.service
fi