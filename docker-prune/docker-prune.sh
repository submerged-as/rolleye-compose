#!/bin/bash

# Configuration
PARTITION="/data/docker"                   # The partition to check (e.g., "/", "/home")
THRESHOLD=90                    # Threshold percentage (without the % sign)
COMMAND="docker volume prune -f ; docker-compose -f /home/submerged/rolleye-compose/docker-compose.yml down ; docker-compose -f /home/submerged/rolleye-compose/docker-compose.yml up -d "  # Command to run when threshold is exceeded
LOGFILE="/data/log/docker_disk_space_checker.log"   # Log file path

# Get the current usage percentage of the partition
USAGE=$(df -h "$PARTITION" | awk 'NR==2 {print $(NF-1)}' | tr -d '%')

# Compare usage with threshold
if [ "$USAGE" -ge "$THRESHOLD" ]; then
    echo "$(date): Disk usage is at ${USAGE}%, exceeding the threshold of ${THRESHOLD}%." | tee -a "$LOGFILE"
    echo "Running command: $COMMAND" | tee -a "$LOGFILE"
    $COMMAND >> "$LOGFILE" 2>&1
else
    echo "$(date): Disk usage is at ${USAGE}%, below the threshold." >> "$LOGFILE"
fi
