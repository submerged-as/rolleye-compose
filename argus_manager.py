import os
import sys
import time
import subprocess
from time import sleep
import socket
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def main():
    host = '0.0.0.0'
    port = 5014

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
        server_socket.bind((host, port))
        server_socket.listen()
        logging.info(f"Server listening on {host}:{port}")

        while True:
            conn, addr = server_socket.accept()
            with conn:
                logging.info(f"Connected by {addr}")
                request = conn.recv(1024).decode().strip().lower()

                if request == "restart":
                    try:
                        subprocess.run(["sudo", "systemctl", "restart", "nvargus-daemon"], check=True)
                        logging.info("Restarted nvargus-daemon successfully")
                        response = "Service restarted successfully"
                    except subprocess.CalledProcessError as e:
                        logging.error(f"Failed to restart nvargus-daemon. Error: {e}")
                        exit(-1)
                    except PermissionError as e:
                        logging.error(f"Permission error: {e}")
                        exit(-2)
                    sleep(5)
                conn.sendall(response.encode())
    logging.info("Exiting...")
if __name__ == "__main__":
    main()